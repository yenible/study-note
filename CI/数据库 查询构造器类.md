#  数据库 查询构造器类

## [加载查询构造器](https://codeigniter.org.cn/user_guide/database/query_builder.html#id16)

查询构造器通过数据库连接对象的 `table()` 方法加载， 这会设置查询语句 `FROM` 的部分并且返回一个查询构造器的新实例:

```
$db      = \Config\Database::connect();
$builder = $db->table('users');
```

查询构造器仅在你明确请求类时才加载到内存中，因此默认不使用（消耗）任何资源。

## [选择数据](https://codeigniter.org.cn/user_guide/database/query_builder.html#id17)

下面的方法用来构建 SQL **SELECT** 语句。

**$builder->get()**

执行选择查询并返回结果，可用于获取一个表的所有记录:

```
$builder = $db->table('mytable');
$query   = $builder->get();  // 生成: SELECT * FROM mytable
```

第一个和第二个参数用于设置 limit 和 offset 子句:

```
$query = $builder->get(10, 20);

// 执行: SELECT * FROM mytable LIMIT 20, 10
// (在 MySQL 里的情况，其他数据库的语法略有不同）
```

你应该已经注意到了，上面方法的结果赋值给了一个 $query 变量， 我们可以用它输出查询结果:

```
$query = $builder->get();

foreach ($query->getResult() as $row)
{
        echo $row->title;
}
```

请访问 [结果方法](https://codeigniter.org.cn/user_guide/database/results.html) 页面获得结果生成的完整论述。