#### CI框架接触学习



##### MVC模型了解

+ M：模型，用于处理数据
+ V:视图，要展现给用户的信息。一个视图通常就是一个网页，但是在 CodeIgniter 中， 一个视图也可以是一部分页面（例如页头、页尾），它也可以是一个 RSS 页面， 或其他任何类型的页面。
+ C：控制器，用于模型、视图以及其他的处理



##### CI项目的URL含义：

URL: 域名/controllers对应文件的路径/controller对应的方法

+ 例：
  + controller文件位置在 ye/test.php,调用方法为xx
  + ![image-20200628090321946](D:\MyData\yenm\AppData\Roaming\Typora\typora-user-images\image-20200628090321946.png)
  + 打开的URL地址为http://play.midea.com/ye/test/xx
  + ![image-20200628090651532](D:\MyData\yenm\AppData\Roaming\Typora\typora-user-images\image-20200628090651532.png)



#### 控制器

+ 类名必须开头字母大写！小写无效

+ URL分段向你的方法传递参数

  + 例如，假设你的 URI 是这样:

    ```
    example.com/index.php/products/shoes/sandals/123
    ```

    你的方法将会收到第三段和第四段两个参数（"sandals" 和 "123"）:

    ```
    <?php
    class Products extends CI_Controller {
    
        public function shoes($sandals, $id)
        {
            echo $sandals;
            echo $id;
        }
    }
    ```

  + 如果你使用了 [*URI 路由*](http://shouce.jb51.net/codeigniter3.0/general/routing.html) ，传递到你的方法的参数将是路由后的参数。

+ 可以定义默认控制器

    + ```php
      $route['default_controller'] = 'blog';
      ```
    
    + 其中，“Blog”是你想加载的控制器类名，如果你现在通过不带任何参数的 index.php 访问你的站点，你将看到你的“Hello World”消息。
    
+ 如果你打算在你的控制器中使用构造函数，你 **必须** 将下面这行代码放在里面:

    ```php
    parent::__construct();
    ```

    原因是你的构造函数将会覆盖父类的构造函数，所以我们要手工的调用它。

+ 





##### 控制器加载视图页面

+ 语句：$this->load->view('name');

  name参数为你的视图文件名

  文件的扩展名.php可以省略，除非使用了其他扩展名

+ 加载多个视图

  CodeIgniter 可以智能的处理在控制器中多次调用 `$this->load->view()` 方法。 如果出现了多次调用，视图会被合并到一起。例如，你可能希望有一个页头视图、 一个菜单视图，一个内容视图 以及 一个页脚视图。代码看起来应该这样:

  ```php
  <?php
  
  class Page extends CI_Controller {
  
      public function index()
      {
          $data['page_title'] = 'Your title';
          $this->load->view('header');
          $this->load->view('menu');
          $this->load->view('content', $data);
          $this->load->view('footer');
      }
  
  }
  ```

+ 页面动态添加数据

  + 通过视图加载方法的第二个参数可以从控制器中动态的向视图传入数据， 这个参数可以是一个 **数组** 或者一个 **对象** 。这里是使用数组的例子:

  + ```php
    <?php
    class Blog extends CI_Controller {
    
        public function index()
        {
            $data['title'] = "My Real Title";
            $data['heading'] = "My Real Heading";
    
            $this->load->view('blogview', $data);
        }
    }
    
    
    ```

  + 视图层加载数据：

    ```php
    <html>
    <head>
        <title><?php echo $title;?></title>
    </head>
    <body>
        <h1><?php echo $heading;?></h1>
    </body>
    </html>
    ```

  + 

##### 将视图作为数据返回

加载视图方法有一个可选的第三个参数可以让你修改它的默认行为，它让视图作为字符串返回 而不是显示到浏览器中，这在你想对视图数据做某些处理时很有用。如果你将该参数设置为 TRUE ， 该方法返回字符串，默认情况下为 FALSE ，视图将显示到浏览器。如果你需要返回的数据， 记住将它赋值给一个变量:

```php
$string = $this->load->view('myfile', '', TRUE);

```





### 模型

+ 如果你想将你的模型对象赋值给一个不同名字的对象，你可以使用 `$this->load->model()` 方法的第二个参数:

  ```
  $this->load->model('model_name', 'foobar');
  
  $this->foobar->method();
  ```






#### 视图



