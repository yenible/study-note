# 正则表达式 RegExp 

### 语法 /正则表达式主体/（修饰符，可选）



### 正则表达式修饰符

**修饰符** 可以在全局搜索中不区分大小写:

| 修饰符 | 描述                                                     |
| :----- | :------------------------------------------------------- |
| i      | 执行对大小写不敏感的匹配。                               |
| g      | 执行全局匹配（查找所有匹配而非在找到第一个匹配后停止）。 |
| m      | 执行多行匹配。                                           |

## 方括号

方括号用于查找某个范围内的字符：

| 表达式                                                       |                描述                |
| :----------------------------------------------------------- | :--------------------------------: |
| [[abc\]](https://www.runoob.com/jsref/jsref-regexp-charset.html) |     查找方括号之间的任何字符。     |
| [[^abc\]](https://www.runoob.com/jsref/jsref-regexp-charset-not.html) |   查找任何不在方括号之间的字符。   |
| [0-9]                                                        |     查找任何从 0 至 9 的数字。     |
| [a-z]                                                        | 查找任何从小写 a 到小写 z 的字符。 |
| [A-Z]                                                        | 查找任何从大写 A 到大写 Z 的字符。 |
| [A-z]                                                        | 查找任何从大写 A 到小写 z 的字符。 |
| [adgk]                                                       |     查找给定集合内的任何字符。     |
| [^adgk]                                                      |     查找给定集合外的任何字符。     |
| (red\|blue\|green)                                           |        查找任何指定的选项。        |

## 元字符

元字符（Metacharacter）是拥有特殊含义的字符：

| 元字符                                                       | 描述                                        |
| :----------------------------------------------------------- | :------------------------------------------ |
| [.](https://www.runoob.com/jsref/jsref-regexp-dot.html)      | 查找单个字符，除了换行和行结束符。          |
| [\w](https://www.runoob.com/jsref/jsref-regexp-wordchar.html) | 查找单词字符。                              |
| [\W](https://www.runoob.com/jsref/jsref-regexp-wordchar-non.html) | 查找非单词字符。                            |
| [\d](https://www.runoob.com/jsref/jsref-regexp-digit.html)   | 查找数字。                                  |
| [\D](https://www.runoob.com/jsref/jsref-regexp-digit-non.html) | 查找非数字字符。                            |
| [\s](https://www.runoob.com/jsref/jsref-regexp-whitespace.html) | 查找空白字符。                              |
| [\S](https://www.runoob.com/jsref/jsref-regexp-whitespace-non.html) | 查找非空白字符。                            |
| [\b](https://www.runoob.com/jsref/jsref-regexp-begin.html)   | 匹配单词边界。                              |
| [\B](https://www.runoob.com/jsref/jsref-regexp-begin-not.html) | 匹配非单词边界。                            |
| \0                                                           | 查找 NULL 字符。                            |
| [\n](https://www.runoob.com/jsref/jsref-regexp-newline.html) | 查找换行符。                                |
| \f                                                           | 查找换页符。                                |
| \r                                                           | 查找回车符。                                |
| \t                                                           | 查找制表符。                                |
| \v                                                           | 查找垂直制表符。                            |
| [\xxx](https://www.runoob.com/jsref/jsref-regexp-octal.html) | 查找以八进制数 xxx 规定的字符。             |
| [\xdd](https://www.runoob.com/jsref/jsref-regexp-hex.html)   | 查找以十六进制数 dd 规定的字符。            |
| [\uxxxx](https://www.runoob.com/jsref/jsref-regexp-unicode-hex.html) | 查找以十六进制数 xxxx 规定的 Unicode 字符。 |

## 量词

| 量词                                                         | 描述                                                         |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| [n+](https://www.runoob.com/jsref/jsref-regexp-onemore.html) | 匹配任何包含至少一个 n 的字符串。例如，/a+/ 匹配 "candy" 中的 "a"，"caaaaaaandy" 中所有的 "a"。 |
| [n*](https://www.runoob.com/jsref/jsref-regexp-zeromore.html) | 匹配任何包含零个或多个 n 的字符串。例如，/bo*/ 匹配 "A ghost booooed" 中的 "boooo"，"A bird warbled" 中的 "b"，但是不匹配 "A goat grunted"。 |
| [n?](https://www.runoob.com/jsref/jsref-regexp-zeroone.html) | 匹配任何包含零个或一个 n 的字符串。例如，/e?le?/ 匹配 "angel" 中的 "el"，"angle" 中的 "le"。 |
| [n{X}](https://www.runoob.com/jsref/jsref-regexp-nx.html)    | 匹配包含 X 个 n 的序列的字符串。例如，/a{2}/ 不匹配 "candy," 中的 "a"，但是匹配 "caandy," 中的两个 "a"，且匹配 "caaandy." 中的前两个 "a"。 |
| [n{X,}](https://www.runoob.com/jsref/jsref-regexp-nxcomma.html) | X 是一个正整数。前面的模式 n 连续出现至少 X 次时匹配。例如，/a{2,}/ 不匹配 "candy" 中的 "a"，但是匹配 "caandy" 和 "caaaaaaandy." 中所有的 "a"。 |
| [n{X,Y}](https://www.runoob.com/jsref/jsref-regexp-nxy.html) | X 和 Y 为正整数。前面的模式 n 连续出现至少 X 次，至多 Y 次时匹配。例如，/a{1,3}/ 不匹配 "cndy"，匹配 "candy," 中的 "a"，"caandy," 中的两个 "a"，匹配 "caaaaaaandy" 中的前面三个 "a"。注意，当匹配 "caaaaaaandy" 时，即使原始字符串拥有更多的 "a"，匹配项也是 "aaa"。 |
| [n$](https://www.runoob.com/jsref/jsref-regexp-ndollar.html) | 匹配任何结尾为 n 的字符串。                                  |
| [^n](https://www.runoob.com/jsref/jsref-regexp-ncaret.html)  | 匹配任何开头为 n 的字符串。                                  |
| [?=n](https://www.runoob.com/jsref/jsref-regexp-nfollow.html) | 匹配任何其后紧接指定字符串 n 的字符串。                      |
| [?!n](https://www.runoob.com/jsref/jsref-regexp-nfollow-not.html) | 匹配任何其后没有紧接指定字符串 n 的字符串。                  |







### 正则中的方法



RegExp的语法：

var patt=new RegExp(pattern,modifiers);

或更简单的方法

var patt=/pattern/modifiers;



#### RegExp对象方法：

+ exec：所见字符串中指定的值，饭hi找到的值，并确定其位置

  + ```js
         let str = 'An apple a day,An apple a day.'
            let reg = new RegExp('(a)')
            let reg2 = /(?<first>\w{2})(?<second>\w{3})/
      
            console.log(reg2.exec(str))
            console.log(reg2.exec(str))
            console.log(reg2.exec(str))
    ```

    输出结果：

![image-20200729155023760](D:\MyData\yenm\AppData\Roaming\Typora\typora-user-images\image-20200729155023760.png)

返回信息分析：

输出的结果为有三个，第一个是匹配到的整个匹配字符，第二三个分别是对应的命名捕获组。

groups：命名捕获组信息，输出的是自定义的捕获组名字以及对应捕获的内容

index:  返回匹配对象的下标位置

input：输入的被匹配的字符串

length： 输出结果的长度



此时没有使用g修饰符，所以连续三次的调用是同一个结果

在使用g后：

```js
let reg2 = /(?<first>\w{2})(?<second>\w{3})/g
```

返回结果：![image-20200729155325064](D:\MyData\yenm\AppData\Roaming\Typora\typora-user-images\image-20200729155325064.png)

返回的结果会在整个字符串进行全局匹配向后匹配



+ test：所见字符串中指定的值，返回true或false

  ```js
      let reg2 = /(?<first>\w{2})(?<second>\w{3})/g
  
  	  console.log(reg2.test(str))
  ```

  输出结果：

  ![image-20200729155546051](D:\MyData\yenm\AppData\Roaming\Typora\typora-user-images\image-20200729155546051.png)

+ toString：返回正则表达式的字符串

```js
let reg2 = /(?<first>\w{2})(?<second>\w{3})/g
        console.log(reg2.toString())
```

返回结果：![image-20200729155803356](D:\MyData\yenm\AppData\Roaming\Typora\typora-user-images\image-20200729155803356.png)





### 支持正则表达式的String对象的方法

+ search（）用于检索字符串中指定的子字符串，或检索与正则表达式相匹配的子字符串，如果没有找到仍和匹配的子串，则返回-1

```js
 let reg2 = /(?<first>\w{2})(?<second>\w{3})/g
        console.log(str.search(reg2))
        console.log(str.search(reg2))
```

返回结果：

![image-20200729160312947](D:\MyData\yenm\AppData\Roaming\Typora\typora-user-images\image-20200729160312947.png)

返回结果是匹配到的子字符串的下标位置，假如匹配不到对应的字符串则返回-1  ！！！



+ match（）可在字符串内检索指定的值，或找到一个或多个正则表达式的匹配。

语法： array.match（regexp）

```js
 let str = 'An apple a day,An apple a day.'
        let reg2 = /(?<first>\w{2})(?<second>\w{3})/g
        console.log(str.match(reg2))        
        console.log(str.match(reg2))		
		console.log(str.match(reg2))
```

返回结果：

![image-20200729161317417](D:\MyData\yenm\AppData\Roaming\Typora\typora-user-images\image-20200729161317417.png)

在不进行全局搜索的时候返回的不单单只是个字符串数组

![image-20200729161407780](D:\MyData\yenm\AppData\Roaming\Typora\typora-user-images\image-20200729161407780.png)



+ replace（）：用于在字符串中用一些字符替换另一些字符，或替换一个与正则表达式匹配的子串。

```js
let str = 'An apple a day,An apple a day.'
        let reg2 = /(?<first>\w{2})(?<second>\w{3})/g
        console.log(str.replace(reg2, 'newWord'))
```

![image-20200729161705176](D:\MyData\yenm\AppData\Roaming\Typora\typora-user-images\image-20200729161705176.png)



总结： RegExp的方法和string方法要区分开调用关系，以及对于是不是要全局g修饰符的区别