# [js array map() 函数的简单使用](https://www.cnblogs.com/oukele/p/10098920.html)

语法：

 1 array.map(function(currentValue,index,arr), thisValue) 

currentValue：必须。当前元素的值

index：可选。当前元素的索引值

**arr：可选。当前元素属于的数组对象**

*****thisValue：可选。对象作为该执行回调时使用，传递给函数，用作 "this" 的值。可改变this指向。*****

 

map() 方法返回一个新数组，数组中的元素为原始数组元素调用函数处理后的值。

map() 方法按照原始数组元素顺序依次处理元素。

**注意：** map() 不会对空数组进行检测。

**注意：** map() 不会改变原始数组。

```js
const numbers = [1, 2, 3, 4, 5];
const doubled = numbers.map((number) => number * 2);
console.log(doubled);
```

代码打印出 `[2, 4, 6, 8, 10]`。

