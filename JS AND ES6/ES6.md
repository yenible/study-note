# ES6学习

#### let与const命令

+ let 特点：

  + 声明的变量仅在块级作用域内有效。

  + let不存在变量提升，即在变量声明之前不会有变量值以及为undefined的情况，假如变量在声明前调用直接报错

  + 暂时性死区：即使在全局存在全局变量tmp，但是在块级作用域内，let重新声明了该变量，导致let绑定了这个块级作用域，所以在这个作用域内的tmp声明前提前调用了tmp，导致赋值报错，使用`let`声明变量时，只要变量在还没有声明完成前使用，就会报错。即使是let x=x也会报错

  + `let`实际上为 JavaScript 新增了块级作用域。

    ```php
    var tmp = 123;
    
    if (true) {
      tmp = 'abc'; // ReferenceError
      let tmp;
    }
    ```

  + let不允许重复声明同个变量

+ 举例

  + 在for循环中输入循环变量，在使用var定义循环变量的时候最终输出都是一样的，使用let定义循环遍历的时候输出的才是有变化的数值。`for`循环还有一个特别之处，就是设置循环变量的那部分是一个父作用域，而循环体内部是一个单独的子作用域。

  + ```js
    var a=[];
            var b=[];
            for(var i=0;i<5;i++){
                console.log(i);
                a[i]=()=>{console.log(i)}
            }
            console.log('*********************')
            for(let i=0;i<5;i++){
                console.log(i);
                b[i]=()=>{console.log(i)}
            }
            console.log('*********************')
    
            a[2]();
            b[2]();
    ```

  + 输出结果

    ![image-20200629115301529](D:\MyData\yenm\AppData\Roaming\Typora\typora-user-images\image-20200629115301529.png)

  + 解释：变量i是var命令声明的，在全局范围内都有效，所以全局只有一个变量i。每一次循环，变量i的值都会发生改变，而循环内被赋给数组a的函数内部的console.log(i)，里面的i指向的就是全局的i。也就是说，所有数组a的成员里面的i，指向的都是同一个i，导致运行时输出的是最后一轮的i的值，也就是 5。

    变量`i`是`let`声明的，当前的`i`只在本轮循环有效，所以每一次循环的`i`其实都是一个新的变量

    

    #### const

+ `const`声明一个只读的常量。一旦声明，常量的值就不能改变。

+ `const`实际上保证的，并不是变量的值不得改动，而是变量指向的那个内存地址所保存的数据不得改动。对于简单类型的数据（数值、字符串、布尔值），值就保存在变量指向的那个内存地址，因此等同于常量。但对于复合类型的数据（主要是对象和数组），变量指向的内存地址，保存的只是一个指向实际数据的指针，`const`只能保证这个指针是固定的（即总是指向另一个固定的地址），至于它指向的数据结构是不是可变的，就完全不能控制了。因此，将一个对象声明为常量必须非常小心。



#### 获取顶层对象

```javascript
// 方法一
(typeof window !== 'undefined'
   ? window
   : (typeof process === 'object' &&
      typeof require === 'function' &&
      typeof global === 'object')
     ? global
     : this);

// 方法二
var getGlobal = function () {
  if (typeof self !== 'undefined') { return self; }
  if (typeof window !== 'undefined') { return window; }
  if (typeof global !== 'undefined') { return global; }
  throw new Error('unable to locate global object');
};
```

+ [ES2020](https://github.com/tc39/proposal-global) 在语言标准的层面，引入`globalThis`作为顶层对象。也就是说，任何环境下，`globalThis`都是存在的，都可以从它拿到顶层对象，指向全局环境下的`this`。



****

### 变量的解构

- ES6 允许按照一定模式，从数组和对象中提取值，对变量进行赋值，这被称为解构（Destructuring）。

- ES6 允许写成下面这样。

  ```javascript
  let [a, b, c] = [1, 2, 3];
  let [foo, [[bar], baz]] = [1, [[2], 3]];
  foo // 1
  bar // 2
  baz // 3
  
  let [ , , third] = ["foo", "bar", "baz"];
  third // "baz"
  
  let [x, , y] = [1, 2, 3];
  x // 1
  y // 3
  
  let [head, ...tail] = [1, 2, 3, 4];
  head // 1
  tail // [2, 3, 4]
  
  let [x, y, ...z] = ['a'];
  x // "a"
  y // undefined
  z // []
  ```

+ 如果解构不成功，变量的值就等于`undefined`。
+ 解构允许放入默认值，ES6 内部使用严格相等运算符（`===`），判断一个位置是否有值。所以，只有当一个数组成员严格等于`undefined`，默认值才会生效。

+ 对象解构，对象的解构与数组有一个重要的不同。数组的元素是按次序排列的，变量的取值由它的位置决定；而对象的属性没有次序，变量必须与属性同名，才能取到正确的值。

  ```js
  let { bar, foo } = { foo: 'aaa', bar: 'bbb' };
  foo // "aaa"
  bar // "bbb"
  
  let { baz } = { foo: 'aaa', bar: 'bbb' };
  baz // undefined
  ```

+ 对象的解构赋值，可以很方便地将现有对象的方法，赋值到某个变量。

```js
// 例一
let { log, sin, cos } = Math;

// 例二
const { log } = console;
log('hello') // hello
```

