语句var arr=[a,b,c,d];执行后，数组arr中每项都是一个整数，下面得到其中最大整数语句正确的是哪几项？

# 正确答案: B C D  你的答案: A B (错误)

```
A、Math.max(arr)
B、Math.max(arr[0], arr[1], arr[2], arr[3])
C、Math.max.call(Math, arr[0], arr[1], arr[2], arr[3])
D、Math.max.apply(Math,arr)
```





Math.max()是求最大值，Math.min()是求最小值

Math.max(value1,value2,value3....)

但是如果是数组或者对象呢？

var numArr = [1,2,4,6,76]

Math.max.apply(null,numArr)

或者 Math.max(...numArr) //76,使用拓展运算符

如果是对象，可以将对象的属性值放入一个数组，然后使用上面两种数组的方法进行求最大最小值。