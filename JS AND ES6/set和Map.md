### 6set

+ 数据结构类似于数组但是成员的值是唯一的，没有重复值

+ Set构造函数用于生产Set数据结构

  ```js
  const s = new Set();
  
  [2, 3, 5, 4, 5, 2, 2].forEach(x => s.add(x));
  
  for (let i of s) {
    console.log(i);
  }
  // 2 3 5 4
  ```

  

+ `Set`函数可以接受一个数组（或者具有 iterable 接口的其他数据结构）作为参数，用来初始化。

  ```js
  // 例一
  const set = new Set([1, 2, 3, 4, 4]);
  [...set]
  // [1, 2, 3, 4]
  
  // 例二
  const items = new Set([1, 2, 3, 4, 5, 5, 5, 5]);
  items.size // 5
  
  // 例三
  const set = new Set(document.querySelectorAll('div'));
  set.size // 56
  
  // 类似于
  const set = new Set();
  document
   .querySelectorAll('div')
   .forEach(div => set.add(div));
  set.size // 56
  ```

+ Set可以用于去除数组的重复成员方法[...new Set(array)，向 Set 加入值的时候，不会发生类型转换，所以`5`和`"5"`是两个不同的值。Set 内部判断两个值是否不同，使用的算法叫做“Same-value-zero equality”，它类似于精确相等运算符（`===`）

|            属性             |                                                |
| :-------------------------: | :--------------------------------------------: |
|  Set.prototype.constructor  |         构造函数，默认就是`Set`函数。          |
|     Set.prototype.size      |           返回`Set`实例的成员总数。            |
|            方法             |                                                |
|  Set.prototype.add(value)   |        添加某个值，返回 Set 结构本身。         |
| Set.prototype.delete(value) | 删除某个值，返回一个布尔值，表示删除是否成功。 |
|  Set.prototype.has(value)   |  返回一个布尔值，表示该值是否为`Set`的成员。   |
|    Set.prototype.clear()    |           清除所有成员，没有返回值。           |

例：

```javascript
s.add(1).add(2).add(2);
// 注意2被加入了两次
```

+ Set对象转为数组    Array.from，结合Array.from和Set去除数组重复的特点，可以封装一个去除数组返回数组的函数

  ```
  const items = new Set([1, 2, 3, 4, 5]);
  const array = Array.from(items);
  ```

+ #### **遍历set操作**

| Set.prototype.keys()      | 返回键名的遍历器         |
| ------------------------- | ------------------------ |
| Set.prototype.entries()   | 返回键值对的遍历器       |
| Set.prototype.values()    | 返回键值的遍历器         |
| `Set.prototype.forEach()` | 使用回调函数遍历每个成员 |

**`Set`的遍历顺序就是插入顺序。这个特性有时非常有用，比如使用 Set 保存一个回调函数列表，调用时就能保证按照添加顺序调用**





### WeakSet

+ WeakSet 结构与 Set 类似，也是不重复的值的集合。但是，它与 Set 有两个区别。

  首先，WeakSet 的成员只能是对象，而不能是其他类型的值

其次，WeakSet 中的对象都是弱引用，即垃圾回收机制不考虑 WeakSet 对该对象的引用，也就是说，如果其他对象都不再引用该对象，那么垃圾回收机制会自动回收该对象所占用的内存，不考虑该对象还存在于 WeakSet 之中。





### Map

+ JavaScript 的对象（Object），本质上是键值对的集合（Hash 结构），但是传统上只能用字符串当作键。这给它的使用带来了很大的限制。

+ ES6 提供了 Map 数据结构。它类似于对象，也是键值对的集合，但是“键”的范围不限于字符串，各种类型的值（包括对象）都可以当作键。也就是说，Object 结构提供了“字符串—值”的对应，Map 结构提供了“值—值”的对应，是一种更完善的 Hash 结构实现。如果你需要“键值对”的数据结构，Map 比 Object 更合适。

+ 增删操作

  ```js
  const m = new Map();
  const o = {p: 'Hello World'};
  
  m.set(o, 'content')
  m.get(o) // "content"
  
  m.has(o) // true
  m.delete(o) // true
  m.has(o) // false
  
  //也可以接受数组作为参数
  const map = new Map([
    ['name', '张三'],
    ['title', 'Author']
  ]);
  
  map.size // 2
  map.has('name') // true
  map.get('name') // "张三"
  map.has('title') // true
  map.get('title') // "Author"
  ```

+ 如果对同一个键多次赋值，后面的值将覆盖前面的值。注意，只有对同一个对象的引用，Map 结构才将其视为同一个键。这一点要非常小心。

```js
const map = new Map();

map.set(['a'], 555);
map.get(['a']) // undefined
```

+ Map 的键实际上是跟内存地址绑定的，只要内存地址不一样，就视为两个键。这就解决了同名属性碰撞（clash）的问题，我们扩展别人的库的时候，如果使用对象作为键名，就不用担心自己的属性与原作者的属性同名。

  如果 Map 的键是一个简单类型的值（数字、字符串、布尔值），则只要两个值严格相等，Map 将其视为一个键，比如`0`和`-0`就是一个键，布尔值`true`和字符串`true`则是两个不同的键。另外，`undefined`和`null`也是两个不同的键。虽然`NaN`不严格相等于自身，但 Map 将其视为同一个键。

```js
let map = new Map();

map.set(-0, 123);
map.get(+0) // 123

map.set(true, 1);
map.set('true', 2);
map.get(true) // 1

map.set(undefined, 3);
map.set(null, 4);
map.get(undefined) // 3

map.set(NaN, 123);
map.get(NaN) // 123
```

+ 实例属性和操作方法

  | **size 属性**                     | 属性返回 Map 结构的成员总数。                                |
  | --------------------------------- | ------------------------------------------------------------ |
  | **Map.prototype.set(key, value)** | `set`方法设置键名`key`对应的键值为`value`，然后返回整个 Map 结构。如果`key`已经有值，则键值会被更新，否则就新生成该键。 |
  | **Map.prototype.get(key)**        | `get`方法读取`key`对应的键值，如果找不到`key`，返回`undefined`。 |
  | **Map.prototype.has(key)**        | `has`方法返回一个布尔值，表示某个键是否在当前 Map 对象之中。 |
  | **Map.prototype.delete(key)**     | const m = new Map();                                         |
  | **Map.prototype.clear()**         | `clear`方法清除所有成员，没有返回值。                        |

+ 遍历方法：

  + 同set......

+ map转为其他的数据结构

  + map转数组使用扩展运算符（...）

    ```js
    const myMap = new Map()
      .set(true, 7)
      .set({foo: 3}, ['abc']);
    [...myMap]
    // [ [ true, 7 ], [ { foo: 3 }, [ 'abc' ] ] ]
    ```

    

  + **数组 转为 Map**:将数组传入 Map 构造函数，就可以转为 Map 

    ```js
    new Map([
      [true, 7],
      [{foo: 3}, ['abc']]
    ])
    ```

    ​	

  + **Map 转为对象** :如果所有 Map 的键都是字符串，它可以无损地转为对象。

    ```js
    function strMapToObj(strMap) {
      let obj = Object.create(null);
      for (let [k,v] of strMap) {
        obj[k] = v;
      }
      return obj;
    }
    
    const myMap = new Map()
      .set('yes', true)
      .set('no', false);
    strMapToObj(myMap)
    ```

    

  + 对象转map对象转为 Map 可以通过`Object.entries()`。

  + map转json，Map 转为 JSON 要区分两种情况。

    + 一种情况是，Map 的键名都是字符串，这时可以选择转为对象 JSON。 

      ```js
      function strMapToJson(strMap) {
        return JSON.stringify(strMapToObj(strMap));
      }
      
      let myMap = new Map().set('yes', true).set('no', false);
      strMapToJson(myMap)
      // '{"yes":true,"no":false}'
      ```

    + **JSON 转为 Map** :JSON 转为 Map，正常情况下，所有键名都是字符串。

      ```javascript
      function jsonToStrMap(jsonStr) {
        return objToStrMap(JSON.parse(jsonStr));
      }
      
      jsonToStrMap('{"yes": true, "no": false}')
      // Map {'yes' => true, 'no' => false}
      ```





### WeakMap

`WeakMap`结构与`Map`结构类似，也是用于生成键值对的集合。

`WeakMap`与`Map`的区别有两点。

首先，`WeakMap`只接受对象作为键名（`null`除外），不接受其他类型的值作为键名。

其次，`WeakMap`的键名所指向的对象，不计入垃圾回收机制。