# 虚拟DOM和diff算法：

 

### DOM和虚拟DOM

+ DOM的本质：浏览器中的概念，用js对象来表示页面商的元素，并提供了操作的DOM对象的API
+ React的虚拟DOM本质:用js来模拟页面DOM元素和嵌套关系
+ 实现虚拟DOM的目的：为了实现页面中，DOM元素的高效更新
+ 一般页面渲染到页面的方式：
  + 手动for循环整个数组，然后手动拼接字符串 str+=‘<><>’
  + 使用模板引擎
+ 如何才能把性能做到最优：按需渲染（只重新渲染更新需要修改的数据，而不是是全部修改）
  + 实现方法：获取内存中的新旧DOM树进行对比，然后按需进行DOM元素的更新
  + 但是浏览器并没有提供直接获取DOM树的API，所以我们需要手动模拟新旧两颗DOM树





### diff算法（different）

+ tree diff ; 新旧两颗DOM树逐层对比的过程，就是tree diff。当整颗DOM树逐层对比完成，则所有需要按需更新的元素，必然能够找到
+ component diff: 在进行tree diff 的时候， 每一层中，组件级别的对比就是component diff。
  + 如果对比前后，组件类型相同，则暂时认为此组件不需要被更新
  + 如果对比前后，组件类型不同，则需要移除旧组件，创建新组件，并追加到页面上。
+ element diff：在进行组件对比的时候，如果两个组件类型相同，则需要进行元素级别的对比，这就是element diff