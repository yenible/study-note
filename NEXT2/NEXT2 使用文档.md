# NEXT2 使用文档

# 前言

NEXT2是在NEXT构建工具的基础上进行大版本升级的产物，可以帮助你更高效率的进行前端项目的开发构建等工程化工作

# NEXT2 特性

NEXT2 相对 NEXT1 主要做的事情是底层的升级，主要的新特性有以下几点，在之前的review上也都有大概阐述，这里不多赘述，总之一句话就是 **更快，更强，更小，更便捷使用，无感升级，无缝切换**

![alt](http://km.midea.com/uploads/imgs/a06524e0d0c2.png)

# NEXT2兼容性

由于底层wp4以及gulp4本身的原因上，NEXT2只能向下兼容，无法向上兼容，也就是说NEXT2不兼容NEXT 1的项目，所以如果将NEXT1的项目复制到NEXT2项目下运行可能会跑不起来，

但是在使用的方式上与NEXT几乎无差别，依旧是基于gulp命令来进行项目任务的监控，打包，编译以及发布，所以童鞋们可以**无缝接入**

# NEXT2 开发指南

### step 1: 更新目录，安装包

1 : svn更新`/static/wp`目录

更新后，会看到 `static/wp/NEXT2/` 文件夹。

2 : 进入 `NEXT2`的文件目录

```
cd static/wp/NEXT2/
```

3 : 执行（请确保已经安装了`node`）

```
npm install
```

4 : 账号配置

在此文件中修改以下字段的为自己的的mip账号，dev账号和密码`static/wp/NEXT2/config/my-config.json`

```
"mipName": "zhangzf16",//你的mip账号
"sftp": {
    "dev": {
      "user": "zhangzf16",//你的dev账号
      "pass": "zhangzf16",//你的dev密码
    },
```

#### step 2: 初始化业务项目

```
npm run create [project name]
```

示例:

```
npm run create myProject
```

你会看到以下提示，说明项目初始化完成。

```
正在创建任务myProject......
myProject项目创建成功~
cd myProject
编译：gulp --page=index
新建页面 gulp new-page --page=index2
```

进入到需要开发的项目目录：

> 可以看到从这里之前NEXT2的开发方式跟之前的基本一致，只是将创建项目由手动复制换成了命令行

```
cd myProject/
```

你会看到有这样的目录结构

> 这里的项目目录结构也与之前没什么区别，只是多了一个cdnconfig.js文件，这个是NEXT2新支持的特性，可以根据这个配置抽取公共库如vue等并在页面中用CND的方式加载，从而达到提高页面加载速度，缩小业务包大小的作用，具体使用如下：

```
├── cdnconfig.js
├── component
│   └── tt
│       ├── main.vue
│       └── tCharts.vue
├── css
│   └── common
│       └── admin-forms.css
├── cssi.html
├── gulpfile.js
├── js
│   └── common.js
├── jsi.html
├── pages
│   └── index.js
└── tpl
    └── index.html
```

请到`static/wp/NEXT2/myProject/cdnconfig.js`文件 中配置项目需要放到cdn的库文件。

例如：项目中用到了`vue,VueResource，element-ui,echarts` 那么配置为：

```
/*@params
* name : npm包名，可在npm官网搜索
* scope : 库对外输出的类名或者模块名
* js : CDN对应文件名
****/
{ name: 'vue', scope: 'Vue', js: 'vue.min.js' },
{ name: 'vue-resource', scope: 'VueResource', js: 'vue-resource.js'},
{ name: 'echarts', scope: 'echarts', js: 'echarts.common.min.js'},
```

编译文件时，终端会输出提示：

![alt](http://km.midea.com/uploads/imgs/253f5b7b7483.png) 目前支持的常用库有：

```
{ name: 'vue', scope: 'Vue', js: 'vue.min.js' },
{ name: 'vue-router', scope: 'VueRouter', js: 'vue-router.min.js' },
{ name: 'axios', scope: 'axios', js: 'axios.min.js' },
{ name: 'element-ui', scope: 'ELEMENT', js: 'index.js', css: 'theme-chalk/index.css' },
{ name: 'echarts', scope: 'echarts', js: 'echarts.common.min.js'},
{ name: 'jquery', scope: 'jQuery', js: 'jquery.min.js'},
{ name: 'vue-resource', scope: 'VueResource', js: 'vue-resource.js'}
```

如果这些常用库，不满足项目需求，请看这篇[NEXT2:常用库的CDN配置](http://km.midea.com/?/article/802)的文章，按步骤新增。

#### step 3: 开发

执行以下命令，可监听文件修改，自动编译。

> 其他gulp打包命令与NEXT一致

```
gulp --page=index
```

#### step4 ：新建页面

> 新命令： 帮你自动生成pages/xx.js以及tpl/xx.html文件

```
gulp new-page --page=YourPageName
```

示例,你需要新建一个名为 `goodslist`的页面,只需要执行：

```
gulp new-page --page=goodslist
```

看到：

```
[19:57:42] Finished 'new-page' after 880 ms
```

这样的提示，说明新建页面成功。

#### 最后，其它的命令

##### 1.`gulp analyz --page=pageName`

打开页面，显示改页面的打包大小的情况，用于分析包大小是否合理 页面示例： ![alt](http://km.midea.com/uploads/imgs/6e2b829cf092.png)

##### 2.`gulp build --page=pageName`

开发完毕后，准备发布前，使用此命令，打包生产环境模式的代码。

##### 3.`gulp pub --page=pageName`

使用此命令，会在把[芭蕉扇](http://oa.midea.com/index.php/publish/searchPublishOrder)生成两个发布单。 例如：

![alt](http://km.midea.com/uploads/imgs/90a92b92e4ce.png)

访问[芭蕉扇](http://oa.midea.com/index.php/publish/searchPublishOrder) **按step1 和 step2顺序发布。**