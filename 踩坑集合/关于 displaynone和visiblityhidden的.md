# 关于 display:none和visiblity:hidden的

# 正确答案: A B  你的答案: A B C (错误)

```
display: none；不为被隐藏的对象保留其物理空间；
visibility:hidden;所占据的空间位置仍然存在,仅为视觉上的完全透明；
visibility:hidden;产生reflow和repaint(回流与重绘)；
visibility:hidden;与display: none;两者没有本质上的区别；
```





Display:none会触发reflow

visibility:hidden会触发repaint