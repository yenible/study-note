# 阻止默认事件

阻止默认事件：

e.preventDefault()

e.returnValue = false (IE)

阻止冒泡：

e.stopPropagation()

e.cancelBubble = true (IE)

