##### function test_input($data) {  $data = trim($data);  $data = stripslashes($data);  $data = htmlspecialchars($data);  return $data; }表单和用户输入

+ PHP 中的 $_GET 和 $_POST 变量用于检索表单中的信息，比如用户输入。

```php
<html>
<head>
<meta charset="utf-8">
<title>菜鸟教程(runoob.com)</title>
</head>
<body>
 
<form action="welcome.php" method="post">
名字: <input type="text" name="fname">
年龄: <input type="text" name="age">
<input type="submit" value="提交">
</form>
 
</body>
</html>
```

welcome.php

```php
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
欢迎<?php echo $_POST["fname"]; ?>!<br>
你的年龄是 <?php echo $_POST["age"]; ?>  岁。

</body>
</html>
```



#### 获取下拉菜单的数据

+ 以下实例我们设置了下拉菜单三个选项，表单使用 GET 方式获取数据，action 属性值为空表示提交到当前脚本，我们可以通过 select 的 name 属性获取下拉菜单的值

  **isset()** 函数用于检测变量是否已设置并且非 NULL。

  ```php
  <?php
  $q = isset($_GET['q'])? htmlspecialchars($_GET['q']) : '';
  if($q) {
          if($q =='RUNOOB') {
                  echo '菜鸟教程<br>http://www.runoob.com';
          } else if($q =='GOOGLE') {
                  echo 'Google 搜索<br>http://www.google.com';
          } else if($q =='TAOBAO') {
                  echo '淘宝<br>http://www.taobao.com';
          }
  } else {
  ?>
  <form action="" method="get"> 
      <select name="q">
      <option value="">选择一个站点:</option>
      <option value="RUNOOB">Runoob</option>
      <option value="GOOGLE">Google</option>
      <option value="TAOBAO">Taobao</option>
      </select>
      <input type="submit" value="提交">
      </form>
  <?php
  }
  ?>
  ```

  

#### 单选框

```php
<?php
$q = isset($_GET['q'])? htmlspecialchars($_GET['q']) : '';
if($q) {
        if($q =='RUNOOB') {
                echo '菜鸟教程<br>http://www.runoob.com';
        } else if($q =='GOOGLE') {
                echo 'Google 搜索<br>http://www.google.com';
        } else if($q =='TAOBAO') {
                echo '淘宝<br>http://www.taobao.com';
        }
} else {
?><form action="" method="get"> 
    <input type="radio" name="q" value="RUNOOB" />Runoob
    <input type="radio" name="q" value="GOOGLE" />Google
    <input type="radio" name="q" value="TAOBAO" />Taobao
    <input type="submit" value="提交">
</form>
<?php
}
?>
```





+ htmlspecialchars() 函数把一些预定义的字符转换为 HTML 实体。

预定义的字符是：

- & （和号） 成为 &amp;
- " （双引号） 成为 &quot;
- ' （单引号） 成为 &#039;
- < （小于） 成为 &lt;
- \> （大于） 成为 &gt;



+ $_SERVER["PHP_SELF"]是超级全局变量，返回当前正在执行脚本的文件名，与 document root相关。所以， $_SERVER["PHP_SELF"] 会发送表单数据到当前页面，而不是跳转到不同的页面。





#### 跨站脚本攻击 xcc（css）

+ $_SERVER["PHP_SELF"] 变量有可能会被黑客使用！_

+ _当黑客使用跨网站脚本的HTTP链接来攻击时，$_SERVER["PHP_SELF"]服务器变量也会被植入脚本。原因就是跨网站脚本是附在执行文件的路径后面的，因此$_SERVER["PHP_SELF"]的字符串就会包含HTTP链接后面的JavaScript程序代码。

  | ![Note](https://www.runoob.com/images/lamp.jpg) | **XSS又叫 CSS (Cross-Site Script) ,跨站脚本攻击。恶意攻击者往Web页面里插入恶意html代码，当用户浏览该页之时，嵌入其中Web里面的html代码会被执行，从而达到恶意用户的特殊目的。** |
  | ----------------------------------------------- | ------------------------------------------------------------ |
  |                                                 |                                                              |

+ **任何JavaScript代码可以添加在<script>标签中！** 黑客可以利用这点重定向页面到另外一台服务器的页面上，页面 代码文件中可以保护恶意代码，代码可以修改全局变量或者获取用户的表单数据。

+ 避免 $_SERVER["PHP_SELF"] 被利用?

  + $_SERVER["PHP_SELF"] 可以通过 htmlspecialchars() 函数来避免被利用。即不单独使用$_SERVER["PHP_SELF"]

    form 代码如下所示：

    ```php
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    ```

    htmlspecialchars() 把一些预定义的字符转换为 HTML 实体。现在如果用户想利用 PHP_SELF 变量, 结果将输出如下所示：

    ```
    <form method="post" action="test_form.php/&quot;&gt;&lt;script&gt;alert('hacked')&lt;/script&gt;">
    ```

    

+ 过滤函数

  ```php
  function test_input($data)
  {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
  ```

  



#### 表单验证邮箱和URL

+ preg_match — 进行正则表达式匹配。
  + 语法：**int preg_match ( string $pattern , string $subject [, array $matches [, int $flags ]] )**