#### 局部作用域和全局作用域

+ 在所有函数外部定义的变量，拥有全局作用域。除了函数外，全局变量可以被脚本中的任何部分访问，要在一个函数中访问一个全局变量，需要使用 global 关键字。

  在 PHP 函数内部声明的变量是局部变量，仅能在函数内部访问：

  ```php+HTML
  <?php 
  $x=5; // 全局变量 
  
  function myTest() 
  { 
      $y=10; // 局部变量 
      echo "<p>测试函数内变量:<p>"; 
      echo "变量 x 为: $x"; 
      echo "<br>"; 
      echo "变量 y 为: $y"; 
  }  
  
  myTest(); 
  
  echo "<p>测试函数外变量:<p>"; 
  echo "变量 x 为: $x"; 
  echo "<br>"; 
  echo "变量 y 为: $y"; 
  ?>
  ```

  + myTest只能输出$y不能输出$x， $x 变量在函数外声明，所以它是全局变量 ， $y 变量在函数内声明所以它是局部变量。
  + 当我们调用myTest()函数并输出两个变量的值, 函数将会输出局部变量 $y 的值，但是不能输出 $x 的值，因为 $x 变量在函数外定义，无法在函数内使用，如果要在一个函数中访问一个全局变量，需要使用 global 关键字。

+ global关键字

  + global 关键字用于函数内访问全局变量。

    在函数内调用函数外定义的全局变量，我们需要在函数中的变量前加上 global 关键字：

    ```php
    <?php
    $x=5;
    $y=10;
     
    function myTest()
    {
        global $x,$y;
        $y=$x+$y;
    }
     
    myTest();
    echo $y; // 输出 15
    ?>
    
    ```

  + PHP 将所有全局变量存储在一个名为 $GLOBALS[*index*] 的数组中。 *index* 保存变量的名称。这个数组可以在函数内部访问，也可以直接用来更新全局变量。

    上面的实例可以写成这样：

    ```php
    <?php
    $x=5;
    $y=10;
     
    function myTest()
    {
        $GLOBALS['y']=$GLOBALS['x']+$GLOBALS['y'];
    } 
     
    myTest();
    echo $y;
    ?>
    ```

+ static作用域

  + 当一个函数完成时，它的所有变量通常都会被删除。然而，有时候您希望某个局部变量不要被删除。

    要做到这一点，请在您第一次声明变量时使用 **static** 关键字：

```
static $x=0;//该变量仍然是函数的局部变量。
```





#### echo与print

+ 区别：
  + echo - 可以输出一个或多个字符串
  + print - 只允许输出一个字符串，返回值总为 1
  + echo 输出的速度比 print 快， echo 没有返回值，print有返回值1。



#### EOF 

+ PHP EOF(heredoc)是一种在命令行shell（如sh、csh、ksh、bash、PowerShell和zsh）和程序语言（像Perl、PHP、Python和Ruby）里定义一个字符串的方法。
+ 使用概述
  + 必须后接分号，否则编译通不过。
  +  **EOF** 可以用任意其它字符代替，只需保证结束标识与开始标识一致。
  + 结束标识必须顶格独自占一行(即必须从行首开始，前后不能衔接任何空白和字符)。**
  +  开始标识可以不带引号或带单双引号，不带引号与带双引号效果一致，解释内嵌的变量和转义符号，带单引号则不解释内嵌的变量和转义符号。
  +  当内容需要内嵌引号（单引号或双引号）时，不需要加转义符，本身对单双引号转义，此处相当与q和qq的用法
  +  PHP 定界符 **EOF** 的作用就是按照原样，包括换行格式什么的，输出在其内部的东西；
  +  在 PHP 定界符 **EOF** 中的任何特殊字符都不需要转义；
  + EOF 中是会解析 html 格式内容的，并且在双引号内的内容也有转义效果。
  + .以 **<<<EOF** 开始标记开始，以 **EOF** 结束标记结束，结束标记必须顶头写，不能有缩进和空格，且在结束标记末尾要有分号 。
  + 开始标记和结束标记相同，比如常用大写的 **EOT、EOD、EOF** 来表示，但是不只限于那几个(也可以用：JSON、HTML等)，只要保证开始标记和结束标记不在正文中出现即可。
  + 位于开始标记和结束标记之间的变量可以被正常解析，但是函数则不可以。在 heredoc 中，变量不需要用连接符 **.** 或 **,** 来拼接，如下：

```php
<?php
$name="runoob";
$a= <<<EOF
        "abc"$name
        "123"
EOF;
// 结束需要独立一行且前后不能空格
echo $a;
?>
```





#### 数据类型

+ 字符串：

```
$x = "Hello world!";
```

+ 整型
  + 整型可以用三种格式来指定：十进制， 十六进制（ 以 0x 为前缀）或八进制（前缀为 0）。
+ 浮点型
  + 浮点数是带小数部分的数字，或是指数形式。

+  布尔型
+  数组
+  对象
+ NULL
  + NULL 值表示变量没有值。NULL 是数据类型为 NULL 的值。
  + NULL 值指明一个变量是否为空值。 同样可用于数据空值和NULL值的区别。
  + 可以通过设置变量值为 NULL 来清空变量数据：





#### 常量

+ 常量是一个简单值的标识符。该值在脚本中不能改变。

+ 一个常量由英文字母、下划线、和数字组成,但数字不能作为首字母出现。 (常量名不需要加 $ 修饰符)。

+ 常量在整个脚本中都可以使用。

+ 设置常量

  + 设置常量，使用 define() 函数，函数语法如下：

    ```
    bool define ( string $name , mixed $value [, bool $case_insensitive = false ] )
    ```

    + **name：**必选参数，常量名称，即标志符。

    + **value：**必选参数，常量的值。

    + **case_insensitive** ：可选参数，如果设置为 TRUE，该常量则大小写不敏感。默认是大小写敏感的。

    + 例子：

      ```php
      <?php
      
      define('USER',"XIAOming");
      define('NAME',"xiaoMING",true);
      echo USER;
      echo "<br>";
      echo NAME;
      
      ?>
      
      <h1>***************************************</h1>
      <?php
          echo USER;
      ?>
      ```

  +   常量是全局的,常量在定义后，默认是全局变量，可以在整个运行的脚本的任何地方使用。






#### 字符串

+ 并置运算

  + 并置运算符 (.) 用于把两个字符串值连接起来。

    ```php
    <?php 
    $txt1="Hello world!"; 
    $txt2="What a nice day!"; 
    echo $txt1 . " " . $txt2; 
    ?>
    ```
    
  + strlen() 函数
  
    + strlen() 函数返回字符串的长度（字节数）。
  
  + strpos() 函数
  
    + strpos() 函数用于在字符串内查找一个字符或一段指定的文本。
  
    + 如果在字符串中找到匹配，该函数会返回第一个匹配的字符位置。如果未找到匹配，则返回 FALSE。
  
      ```
          echo strpos("Hello world!","world"); //返回6
      ```
  
      

    #### if...else   if...elseif....else 





####  Switch 语句





#### 数组

+ 数组创建：

  + array() 函数用于创建数组：

  + 在 PHP 中，有三种类型的数组：

    - **数值数组** - 带有数字 ID 键的数组
    - **关联数组** - 带有指定的键的数组，每个键关联一个值
    - **多维数组** - 包含一个或多个数组的数组

  + 数值数组(两种创建方法)：

    + 自动分配 ID 键（ID 键总是从 0 开始）：

      ```php+HTML
      $cars=array("Volvo","BMW","Toyota");
      ```

    + 人工分配 ID 键：

      ```php+HTML
      $cars[0]="Volvo";
      $cars[1]="BMW";
      ```

  + 遍历数值数组 可以使用for

  + 关联数组

    + 关联数组是使用您分配给数组的指定的键的数组。

    + 创建方法（两种）

      + ```php+HTML
        $age=array("Peter"=>"35","Ben"=>"37","Joe"=>"43");
        ```

      + ```php+HTML
        $age['Peter']="35";
        $age['Ben']="37";
        ```

  + 遍历关联数组使用foreach

    ```php+HTML
    <?php
    $age=array("Peter"=>"35","Ben"=>"37","Joe"=>"43");
     
    foreach($age as $x=>$x_value)
    {
        echo "Key=" . $x . ", Value=" . $x_value;
        echo "<br>";
    }
    ?>
    ```

    

  + 获取数组的长度 - count() 函数

    ```php+HTML
    <?php
    $cars=array("Volvo","BMW","Toyota");
    echo count($cars);
    ?>
    ```

  

  

  #### 数组排序函数

  + sort() - 对数组进行升序排列
  + rsort() - 对数组进行降序排列
  + asort() - 根据关联数组的值，对数组进行升序排列
  + ksort() - 根据关联数组的键，对数组进行升序排列
  + arsort() - 根据关联数组的值，对数组进行降序排列
  + krsort() - 根据关联数组的键，对数组进行降序排列



#### 超级全局变量

+ $GLOBALS

  + $GLOBALS 是PHP的一个超级全局变量组，在一个PHP脚本的全部作用域中都可以访问。

    $GLOBALS 是一个包含了全部变量的全局组合数组。变量的名字就是数组的键。

+ $_SERVER

  + $_SERVER 是一个包含了诸如头信息(header)、路径(path)、以及脚本位置(script locations)等等信息的数组。这个数组中的项目由 Web 服务器创建。不能保证每个服务器都提供全部项目；服务器可能会忽略一些，或者提供一些没有在这里列举出来的项目。

+ $_REQUEST

  + PHP $_REQUEST 用于收集HTML表单提交的数据。

+ PHP $_POST

  + PHP $_POST 被广泛应用于收集表单数据，在HTML form标签的指定该属性："method="post"。

  + ```php+HTML
    <html>
    <body>
     
    <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
    Name: <input type="text" name="fname">
    <input type="submit">
    </form>
     
    <?php 
    $name = $_POST['fname']; 
    echo $name; 
    ?>
     
    </body>
    </html>
    ```

+ PHP $_GET

  + PHP $_GET 同样被广泛应用于收集表单数据，在HTML form标签的指定该属性："method="get"。

  $_GET 也可以收集URL中发送的数据。





#### 循环

+ for循环
+ foreach
+ while
+ do..while



#### 函数

+ 创建函数

  + ```php
    <?php
    function functionName()
    {
        // 要执行的代码
    }
    ?>
    ```

  + 如需让函数返回一个值，请使用 return 语句。





#### 魔术常量

+ PHP 向它运行的任何脚本提供了大量的预定义常量。

  不过很多常量都是由不同的扩展库定义的，只有在加载了这些扩展库时才会出现，或者动态加载后，或者在编译时已经包括进去了。

+ \_\_LINE\_\_

  + 文件中的当前行号。

+ \_\_FILE\_\_
  文件的完整路径和文件名。如果用在被包含文件中，则返回被包含的文件名。

+ \_\_DIR\_\_
  文件所在的目录。如果用在被包括文件中，则返回被包括的文件所在的目录。

  它等价于 dirname(__FILE__)。除非是根目录，否则目录中名不包括末尾的斜杠。（PHP 5.3.0中新增）

+ \__FUNCTION__
  函数名称（PHP 4.3.0 新加）。自 PHP 5 起本常量返回该函数被定义时的名字（区分大小写）。在 PHP 4 中该值总是小写字母的。

  ```php
  <?php
  function test() {
      echo  '函数名为：' . __FUNCTION__ ;
  }
  test();
  ?>
  ```

+ \__CLASS__
  类的名称（PHP 4.3.0 新加）。自 PHP 5 起本常量返回该类被定义时的名字（区分大小写）。

+ \__TRAIT__????
  Trait 的名字（PHP 5.4.0 新加）。自 PHP 5.4.0 起，PHP 实现了代码复用的一个方法，称为 traits。

  Trait 名包括其被声明的作用区域（例如 Foo\Bar）。

  从基类继承的成员被插入的 SayWorld Trait 中的 MyHelloWorld 方法所覆盖。其行为 MyHelloWorld 类中定义的方法一致。优先顺序是当前类中的方法会覆盖 trait 方法，而 trait 方法又覆盖了基类中的方法。

+ \__METHOD__
  类的方法名（PHP 5.0.0 新加）。返回该方法被定义时的名字（区分大小写）。

+ \__NAMESPACE__
  当前命名空间的名称（区分大小写）。此常量是在编译时定义的（PHP 5.3.0 新增）。







#### 命名空间

+ PHP 命名空间可以解决以下两类问题：
  1. 用户编写的代码与PHP内部的类/函数/常量或第三方类/函数/常量之间的名字冲突。
  2. 为很长的标识符名称(通常是为了缓解第一类问题而定义的)创建一个别名（或简短）的名称，提高源代码的可读性。

+ 定义命名空间

  + 默认情况下，所有常量、类和函数名都放在全局空间下，就和PHP支持命名空间之前一样。

  + 命名空间通过关键字namespace 来声明。如果一个文件中包含命名空间，它必须在其它所有代码之前声明命名空间。语法格式如下；

```
?php  
// 定义代码在 'MyProject' 命名空间中  
namespace MyProject;  
 
// ... 代码 ...  
```

+ ​	也可以在同一个文件中定义不同的命名空间代码	

+ 不建议使用这种语法在单个文件中定义多个命名空间。建议使用下面的大括号形式的语法。

  + ```php
    <?php
    namespace MyProject {
        const CONNECT_OK = 1;
        class Connection { /* ... */ }
        function connect() { /* ... */  }
    }
    
    namespace AnotherProject {
        const CONNECT_OK = 1;
        class Connection { /* ... */ }
        function connect() { /* ... */  }
    }
    ?>
    ```

+ 在声明命名空间之前唯一合法的代码是用于定义源文件编码方式的 declare 语句。所有非 PHP 代码包括空白符都不能出现在命名空间的声明之前。

  + 以下代码会出现语法错误

```php
<html>
<?php
namespace MyProject; // 命名空间前出现了“<html>” 会致命错误 -　命名空间必须是程序脚本的第一条语句
?>
```

+ namespace关键字和\__NAMESPACE__常量
  PHP支持两种抽象的访问当前命名空间内部元素的方法，\_\_NAMESPACE\_\_ 魔术常量和namespace关键字。

  常量\__NAMESPACE__的值是包含当前命名空间名称的字符串。在全局的，不包括在任何命名空间中的代码，它包含一个空的字符串。

  \__NAMESPACE__ 示例, 在命名空间中的代码

+ 关键字 namespace 可用来显式访问当前命名空间或子命名空间中的元素。它等价于类中的 self 操作符。

  namespace操作符，命名空间中的代码







#### 对象

```php
<?php
class Site {
  /* 成员变量 */
  var $url;
  var $title;
  
  /* 成员函数 */
  function setUrl($par){
     $this->url = $par;
  }
  
  function getUrl(){
     echo $this->url . PHP_EOL;
  }
  
  function setTitle($par){
     $this->title = $par;
  }
  
  function getTitle(){
     echo $this->title . PHP_EOL;
  }
}
?>
```



+ 使用 **class** 关键字后加上类名定义。

+ 类名后的一对大括号({})内可以定义变量和方法。

+ 类的变量使用 **var** 来声明, 变量也可以初始化值。

+ 函数定义类似 PHP 函数的定义，但函数只能通过该类及其实例化的对象访问。

+ 变量 **$this** 代表自身的对象。

  **PHP_EOL** 为换行符。



+ 实例类对象，使用new

```
$runoob = new Site;
$taobao = new Site;
$google = new Site;
```



+ 使用成员方法

  ```php
  // 调用成员函数，设置标题和URL
  $runoob->setTitle( "菜鸟教程" );
  $taobao->setTitle( "淘宝" );
  $google->setTitle( "Google 搜索" );
  
  $runoob->setUrl( 'www.runoob.com' );
  $taobao->setUrl( 'www.taobao.com' );
  $google->setUrl( 'www.google.com' );
  
  // 调用成员函数，获取标题和URL
  $runoob->getTitle();
  $taobao->getTitle();
  $google->getTitle();
  
  $runoob->getUrl();
  $taobao->getUrl();
  $google->getUrl();
  ```

  

##### 构造函数

+ 构造函数是一种特殊的方法。主要用来在创建对象时初始化对象， 即为对象成员变量赋初始值，在创建对象的语句中与 **new**运算符一起使用。

+ 构造函数语法：

  ```
  void __construct ([ mixed $args [, $... ]] )
  ```

+ 在上面的例子中我们就可以通过构造方法来初始化 $url 和 $title 变量：

  ```php
  function __construct( $par1, $par2 ) {
     $this->url = $par1;
     $this->title = $par2;
  }
  ```

  实例：

  ```php
  $runoob = new Site('www.runoob.com', '菜鸟教程'); 
  $taobao = new Site('www.taobao.com', '淘宝'); 
  $google = new Site('www.google.com', 'Google 搜索'); 
  
  // 调用成员函数，获取标题和URL 
  $runoob->getTitle(); 
  $taobao->getTitle(); 
  $google->getTitle(); 
  
  $runoob->getUrl(); 
  $taobao->getUrl(); 
  $google->getUrl();
  ```

  

##### 析构函数

+ 析构函数(destructor) 与构造函数相反，当对象结束其生命周期时（例如对象所在的函数已调用完毕），系统自动执行析构函数。

```
void __destruct ( void )
```

实例：

```php
<?php
class MyDestructableClass {
   function __construct() {
       print "构造函数\n";
       $this->name = "MyDestructableClass";
   }

   function __destruct() {
       print "销毁 " . $this->name . "\n";
   }
}

$obj = new MyDestructableClass();
?>
```

输出结果

```
构造函数
销毁 MyDestructableClass
```





##### 继承

+ PHP 使用关键字 **extends** 来继承一个类，PHP 不支持多继承，格式如下：

```
class Child extends Parent {
   // 代码部分
}
```



##### 方法重写

+ 如果从父类继承的方法不能满足子类的需求，可以对其进行改写，这个过程叫方法的覆盖（override），也称为方法的重写。

  实例中重写了 getUrl 与 getTitle 方法：

````
function getUrl() {
   echo $this->url . PHP_EOL;
   return $this->url;
}
   
function getTitle(){
   echo $this->title . PHP_EOL;
   return $this->title;
}
````







##### 访问控制

+ PHP 对属性或方法的访问控制，是通过在前面添加关键字 public（公有），protected（受保护）或 private（私有）来实现的。
  - **public（公有）：**公有的类成员可以在任何地方被访问。
  - **protected（受保护）：**受保护的类成员则可以被其自身以及其子类和父类访问。
  - **private（私有）：**私有的类成员则只能被其定义所在的类访问。





##### 接口

+ 使用接口（interface），可以指定某个类必须实现哪些方法，但不需要定义这些方法的具体内容。
+ 接口是通过 **interface** 关键字来定义的，就像定义一个标准的类一样，但其中定义所有的方法都是空的。
+ 接口中定义的所有方法都必须是公有，这是接口的特性
+ 要实现一个接口，使用 **implements** 操作符。类中必须实现接口中定义的所有方法，否则会报一个致命错误。类可以实现多个接口，用逗号来分隔多个接口的名称。





##### 类的常量

+ 可以把在类中始终保持不变的值定义为常量。在定义和使用常量的时候不需要使用 $ 符号。

  常量的值必须是一个定值，不能是变量，类属性，数学运算的结果或函数调用。

  自 PHP 5.3.0 起，可以用一个变量来动态调用类。但该变量的值不能为关键字（如 self，parent 或 static）。





##### 抽象类

任何一个类，如果它里面至少有一个方法是被声明为抽象的，那么这个类就必须被声明为抽象的。

定义为抽象的类不能被实例化。

被定义为抽象的方法只是声明了其调用方式（参数），不能定义其具体的功能实现。

继承一个抽象类的时候，子类必须定义父类中的所有抽象方法；另外，这些方法的访问控制必须和父类中一样（或者更为宽松）。例如某个抽象方法被声明为受保护的，那么子类中实现的方法就应该声明为受保护的或者公有的，而不能定义为私有的。





##### static

声明类属性或方法为 static(静态)，就可以不实例化类而直接访问。

静态属性不能通过一个类已实例化的对象来访问（但静态方法可以）。

由于静态方法不需要通过对象即可调用，所以伪变量 $this 在静态方法中不可用。

静态属性不可以由对象通过 -> 操作符来访问。

自 PHP 5.3.0 起，可以用一个变量来动态调用类。但该变量的值不能为关键字 self，parent 或 static。



##### Final 关键字

PHP 5 新增了一个 final 关键字。如果父类中的方法被声明为 final，则子类无法覆盖该方法。如果一个类被声明为 final，则不能被继承。

 

##### 调用父类构造方法

PHP 不会在子类的构造方法中自动的调用父类的构造方法。要执行父类的构造方法，需要在子类的构造方法中调用 **parent::__construct()** 。