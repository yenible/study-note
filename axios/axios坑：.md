axios坑：

+ post请求，数据发送出去但是后台无法获取

  + 解决方法：

    + 加个请求头{headers:{'Content-Type':'application/x-www-form-urlencoded'}

    + 格式化数据

      + ```js
        
        //     var params = new URLSearchParams();
        //         params.append('name', value['name']);
        //         params.append('finishDate', value['finishDate']);
        //         params.append('content', value['content']);
        ```

      + npm安装qs 使用qs.stringfy格式化数据

        ```js
         instance.post('/addTask', qs.stringify(value)).then(res => {
                console.log(res);
                console.log('发送22成功'); 
            }).catch((res) => {
                console.log(res);
            })
        ```

        

